from typing import NamedTuple, Union
from django.contrib.auth import authenticate, login
from .forms import LoginForm, CreatingUserForm


class Forms(NamedTuple):
    login_form: LoginForm
    new_user: CreatingUserForm

forms = Forms(
    login_form = LoginForm(),
    new_user = CreatingUserForm(),
)


def register_worker(request) -> dict:
    if request.method == "POST":
        form = CreatingUserForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            return {
                'url': 'register_done.html',
                'data': {
                    'new_user': new_user,
                }
            }
    else:
        return {
            'url': 'register.html',
            'data': {
                'user_form': CreatingUserForm()
            }
        }

# class Report(Enum):
#     SUCCESS = 'Authenticated successfully'
#     DISABLED = 'Disabled account'
#     INVALID = 'Invalid login or password' 
# 
# 
# def logining(request) -> dict:
#     report = ''
#     ret_dict = dict()
#     ret_dict['report'] = report
#     ret_dict['login_form'] = LoginForm()
#     if request.method == "POST":
#         login_form = LoginForm(request.POST)
#         ret_dict['login_form'] = login_form
#         if ret_dict['login_form'].is_valid():
#             cd = ret_dict['login_form'].cleaned_data
#             user = authenticate(request,
#                                 username=cd['username'],
#                                 password=cd['password'])
#             if user:
#                 if user.is_active:
#                     login(request, user)
#                     ret_dict['report'] = Report.SUCCESS
#                     return ret_dict
#                 else:
#                     ret_dict['report'] = Report.DISABLED
#                     return ret_dict
#             else:
#                 ret_dict['report'] = Report.INVALID
#                 return ret_dict
#     return ret_dict
