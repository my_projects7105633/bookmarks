from django.shortcuts import render
from .services import register_worker
from django.contrib.auth.decorators import login_required


#def user_login(request):
#    data_dict = logining(request)
#    url = ''
#    if data_dict['report'] in (Report.DISABLED, Report.INVALID):
#        url = 'account/login.html'
#    else:
#        url = 'account/home.html'
#    return render(request,
#                  url,
#                  data_dict)


@login_required
def dashboard(request):
    return render(request,
                  'account/dashboard.html',
                  {'section': 'dashboard'})

def registration(request):
    data_dict = register_worker(request)
    return render(request,
                  data_dict['url'],
                  data_dict['data'])
