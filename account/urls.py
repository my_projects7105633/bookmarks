from django.urls import include, path
from django.contrib.auth import views as auth_view
from . import views


urlpatterns = [
    # #path('login/', views.user_login, name='login'),
    # #path('home/', views., name='home'),
    # path('login/', auth_view.LoginView.as_view(), name='login'),
    # path('logout/', auth_view.LogoutView.as_view(), name='logout'),
    # # Password changing
    # path('password-change/',
    #      auth_view.PasswordChangeView.as_view(),
    #      name='password_change'),
    # path('password-change/done/',
    #      auth_view.PasswordChangeDoneView.as_view(),
    #      name='password_change_done'),
    # # Reset password
    # path('password-reset/',
    #      auth_view.PasswordResetView.as_view(),
    #      name='password_reset'),
    # path('password-reset/done/',
    #      auth_view.PasswordResetDoneView.as_view(),
    #      name='password_reset_done'),
    # path('password-reset/confirm/<uidb64>/<token>/',
    #      auth_view.PasswordResetConfirmView.as_view(),
    #      name='password_reset_confirm'),
    # path('password-reset/complete/',
    #      auth_view.PasswordResetCompleteView.as_view(),
    #      name='password_reset_complete'),
    # # Main page
    path('', include('django.contrib.auth.urls')),
    path('', views.dashboard, name='dashboard'),
    path('register/', views.registration, name='register'),
]
